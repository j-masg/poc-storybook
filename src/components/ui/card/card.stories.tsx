import type { Meta, StoryObj } from '@storybook/react';

import { Card } from './card';
import { Trash2, Zap } from 'lucide-react';

const meta = {
    title: 'UI/Card',
    component: Card,
    parameters: {
        layout: 'centered'
    },
    tags: ['autodocs']
} satisfies Meta<typeof Card>

export default meta;
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        title: 'Lorem ipsum dolor sit amet.',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
        children: (
            <>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem provident officiis explicabo eum repudiandae consequuntur.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste delectus laboriosam exercitationem nostrum enim? Non sit nam ipsum!</p>
            </>
        ),
    }
}

export const WithButton: Story = {
    args: {
        title: 'Lorem ipsum dolor sit amet.',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
        children: (
            <>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem provident officiis explicabo eum repudiandae consequuntur.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste delectus laboriosam exercitationem nostrum enim? Non sit nam ipsum!</p>
            </>
        ),
        buttonLabel: 'Lorem Ipsum',
        buttonIcon: <Zap />
    }
}

export const WithSecondaryButton: Story = {
    args: {
        title: 'Lorem ipsum dolor sit amet.',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
        children: (
            <>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem provident officiis explicabo eum repudiandae consequuntur.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste delectus laboriosam exercitationem nostrum enim? Non sit nam ipsum!</p>
            </>
        ),
        buttonLabel: 'Lorem Ipsum',
        buttonVariant: 'secondary'
    }
}

export const WithDestructiveButton: Story = {
    args: {
        title: 'Lorem ipsum dolor sit amet.',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
        children: (
            <>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem provident officiis explicabo eum repudiandae consequuntur.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste delectus laboriosam exercitationem nostrum enim? Non sit nam ipsum!</p>
            </>
        ),
        buttonLabel: 'Lorem Ipsum',
        buttonVariant: 'destructive',
        buttonIcon: <Trash2 />
    }
}