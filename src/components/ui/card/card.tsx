import * as React from 'react';
import { Button } from '../button/button';

interface CardProps {
    title: string;
    description?: string;
    buttonLabel?: string;
    buttonVariant?: 'primary' | 'secondary' | 'destructive';
    buttonIcon?: React.ReactNode;
    children?: React.ReactNode
}

export const Card = ({
    title,
    description,
    buttonLabel,
    buttonVariant,
    buttonIcon,
    ...props
}: CardProps) => {
    return (
        <article className='rounded-xl border border-slate-200 p-4 shadow-xl overflow-hidden'>
            <h2 className='font-bold text-3xl mb-2'>{title}</h2>
            { description ? <p className='text-slate-600'>{description}</p> : '' }
            <div className='my-4 mt-6 flex flex-col gap-2'>
                {props.children}
            </div>
            <div className='flex flex-row-reverse mt-6'>
                { buttonLabel ? <Button IconProp={buttonIcon} label={buttonLabel} variant={buttonVariant} /> : '' }
            </div>
        </article>
    )
}

