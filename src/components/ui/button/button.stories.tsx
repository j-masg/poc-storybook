import type { Meta, StoryObj } from '@storybook/react';

import { Button } from './button';
import { Heart, LucideIcon } from 'lucide-react';

const meta = {
    title: 'UI/Button',
    component: Button,
    parameters: {
        layout: 'centered'
    },
    tags: ['autodocs'],
    argTypes: {
        variant: { control: 'select' },
        size: { control: 'select' },
        IconProp: { control: 'text' }
    }
} satisfies Meta<typeof Button>

export default meta;
type Story = StoryObj<typeof meta>

export const Primary: Story = {
    args: {
        label: 'Button'
    }
}

export const Small: Story = {
    args: {
        variant: 'primary',
        label: 'Button',
        size: 'sm'
    }
}

export const Large: Story = {
    args: {
        variant: 'primary',
        label: 'Button',
        size: 'lg'
    }
}

export const WithIcon: Story = {
    args: {
        variant: 'primary',
        label: 'Button',
        withIcon: true,
        IconProp: <Heart />
    }
}

export const Secondary: Story = {
    args: {
        variant: 'secondary',
        label: 'Button'
    }
}

export const Destructive: Story = {
    args: {
        variant: 'destructive',
        label: 'Button'
    }
}

export const Disabled: Story = {
    args: {
        label: 'Button',
        disabled: true
    }
}

export const Ghost: Story = {
    args: {
        variant: 'ghost',
        label: 'Button'
    }
}

export const Link: Story = {
    args: {
        variant: 'link',
        label: 'Button'
    }
}