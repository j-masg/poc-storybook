import * as React from 'react'

interface ButtonProps {
    /**
     * Button contents
     */
    label: string;
    /**
     * Variante du bouton
     */
    variant?: 'primary' | 'secondary' | 'ghost' | 'destructive' | 'link';
    /**
     * Le bouton est-il désactivé
     */
    disabled?: boolean;
    /**
     * La taille du bouton
     */
    size?: 'sm' | 'default' | 'lg';
    /**
     * Le bouton contient-il une icone ?
     */
    withIcon?: boolean;
    /**
     * L'icone à afficher
     */
    IconProp?: React.ReactNode;
    /**
     * Action à effectuer au click
     * @returns void
     */
    onClick?: () => void
}

let generalStyles = 'rounded-xl flex gap-4'

export const Button = ({
    variant = 'primary',
    size = 'default',
    disabled = false,
    IconProp = undefined,
    label,
    ...props
}: ButtonProps) => {
    console.log('!===')
    console.log({variant, size, disabled, IconProp, label})
    let variantStyle = '';
    switch(variant) {
        case 'primary':
            variantStyle = 'bg-ad-primary text-white hover:bg-ad-primary-lighter';
            break;
        case 'secondary': 
            variantStyle = 'bg-ad-secondary text-slate-800 hover:bg-ad-secondary-lighter';
            break;
        case 'ghost':
            variantStyle = 'bg-transparent text-slate-800 hover:bg-ad-secondary-lighter';
            break;
        case 'destructive':
            variantStyle = 'bg-ad-destructive text-white hover:bg-ad-destructive-lighter';
            break;
        case 'link':
            variantStyle = 'bg-transparent color-slate-800 hover:underline'
    }

    if (disabled) {
        variantStyle = 'bg-ad-secondary-lighter text-slate-400'
    }

    let sizeStyle = ''
    switch(size) {
        case 'sm': 
            sizeStyle = 'py-2 px-4';
            break;
        case 'default':
            sizeStyle = 'py-3 px-6';
            break;
        case 'lg': 
            sizeStyle = 'py-4 px-10';
            break;
    }

    console.log('hoho')
    const classList = generalStyles + ' ' + variantStyle + ' ' + sizeStyle
    console.log(classList)
    console.log('--')

    return (
        <button className={classList} disabled={disabled} {...props}
        >
            {IconProp ? IconProp : ''}
            {label}
        </button>
    )
}